// Find a pair of number where sum of two number is the input "sum".

// ex: 
/**
For sum = 8
    [1,1,2,3,8]  // single digit is sum
    [1,2,3,4,4]  // double digit is sum
    [1,1,2,3,4]  // No pairs
Input: sum, array
Output: bool pairThere, pair sumPair 
**/

/**
TestCase
  8 + 1 = 9  // greater than sum   decrease higherPtr
  3 + 1 = 4  // less than sum      increase lowerPtr
  3 + 2= 5   // less than sum      increase lowerPtr

  1 + 4 = 5 // less than sum      increase lowerPtr
  2+ 4 = 6
  3+ 4  = 7
  4+4 =8

  **/

#include <iostream>

std::pair<bool,std::pair<uint64_t, uint64_t>> findPair(uint64_t i_sum,  uint64_t *arr, uintmax_t i_size)
{
  // Check for input correctness
  // TODO: Check if the size < uintmax_t size

  // Initialize two index pointers unsigned int
  uintmax_t lowerIndex = 0;
  uintmax_t higherIndex = i_size-1;
  std::pair <bool, std::pair <uint64_t, uint64_t>> o_pair;  
  for ( ; lowerIndex < higherIndex ; )
  {
      uintmax_t sumPair  =  arr[lowerIndex] + arr[higherIndex];
      // TODO: check for overflow      
      if (sumPair == i_sum)
      {
          // success, Return true and pair
          o_pair.first = true;
          o_pair.second = std::make_pair(arr[lowerIndex], arr[higherIndex]);
          return o_pair;
      }
      else if (sumPair < i_sum)
      {
          lowerIndex++;
      }
      else //(sumPair > i_sum)
      {
          higherIndex --;
      }

  }
  // return false, empty pair
  o_pair.first = false;
  o_pair.second = std::make_pair(0,0);
  return o_pair;
  // TODO: Test if unisigned int has to made for 0s
}

int main() {
  
  uint64_t arr[5] = {1,1,2,3,4};
  uint64_t size = 5;
  uintmax_t sum = 8;

  std::pair<bool,std::pair<uint64_t, uint64_t>> pair;
  pair = findPair(sum, arr, size);
  if (pair.first)
  {
    std::cout << pair.second.first << " + "<< pair.second.second;
  }else
  {
    std::cout << "No pair found whose sum is " << sum;

  }
    return 0;
}