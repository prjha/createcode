// Author: Pranav Prakash Jha
// File: starter.cpp
// Purpose: To represent basic IO operations

#include <iostream>

int main() {
	std::cout << "This is our first program\n";
	std::cout << "All done!\n";
}
