// To create a dynamic array implementation that is available in STL library
// Here we are going to create a class for it.
// We will support following options:
/**
 * 1. Push
 * 2. Pop
 * 3. Access
 * 4. Delete
 **/

#include <iostream>
#include <bits/stdc++.h>

class DynamicArray
{
    // Member variables
    int size;
    int *arr;
    int capacity = 2;    
public:
    DynamicArray()
    {
        // Initialize the size
        arr= new int[2];
        size = 0;
        capacity = 2;
    }
    ~DynamicArray()
    {
        // Delete all elements
        delete[] arr;
        size = 0;
        capacity = 0;
    }

    void push (int i_input)
    {
        
        if (size >= capacity)
        {
            std::cout << "\nSize equals capacity: " << capacity; 
            capacity = capacity * 2;
            int *temp = new int [capacity];
            copy(arr, temp , size);
            arr = temp;
        }
        size ++;
        arr[size-1] = i_input;
        print();
    }

    static void copy (int * arrFrom, int *arrTo, int size)
    {
        for (int i = 0; i < size ; i ++)
        {
            arrTo[i] = arrFrom[i];
        }
    }
    int pop ()
    {
        int toReturn = arr[size-1];
        size--;
        return toReturn;
    }

    void print ()
    {
        std::cout << "\n";
        for (int i = 0 ; i < size; i++)
        {
            std::cout << arr[i] << " ";
        }
    }
};

int main(int argc, char const *argv[])
{
    DynamicArray dynArr;
    dynArr.push(5);
    dynArr.push(3);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.push(15);
    dynArr.pop();
    dynArr.print();
    return 0;
}
