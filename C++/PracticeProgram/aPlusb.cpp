// Author: Pranav Prakash Jha
// File: aPlusb.cpp
// Purpose: https://www.hackerearth.com/practice/basic-programming/complexity-analysis/time-and-space-complexity/practice-problems/algorithm/a-b-4/description/

/*
Problem Description
Given a series of integer pairs A and B, output the sum of A and B.

Input Format
Each line contains two integers, A and B. One input file may contain several pairs  where $$0 \leq P \leq 12$$.

Output Format
Output a single integer per line - The sum of A and B.

Constraints
$$0 <= A,B <= 10^{98}$$
*/

/*
Test Cases:
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
1983479127394712739417293471297349712347812 12938471297349127394791273497123471297341927
9283479273497239472934729378492734 2398472938479237492374927947293749274
19191919191919191 19191919191919191919191919191919191919191919191919191919191919191919191
98919283791723912793719873971293791283791273123 1928371927391723971927391737172937
0 0
0 1
*/

#include<iostream>
#include<string>
using namespace std;
 
string addString( string num1, string num2 )
{
    //DEBUG// cout << "Add (" << num1 << ", " << num2 << ")" << endl;
    // Prerequisites-----------------------
    // Pre. 1.: Both entries should not be empty
    if (num1.empty())
    {
        if (num2.empty())
            return ""; // num1 & num2 empty
        else
            return num2; // num1 empty, num 2 valid
    }
    else if (num2.empty())
        return num1; // num1 valid, num 2 empty

    // Pre.2.: We are going to take the longest one as num1
    if (num1.length() < num2.length())
    {
        swap(num1, num2);
        num1_dup = num2;
    }
    // Prerequisites ------ END -----------

    int index_1 = num1.length() -1; 
    for (int index = num2.length() - 1; index >= 0 ; --index, --index_1)
    {
        num1[index_1] = num1[index_1] - '0' + num2[index];
    }
    
    for(index_1 = num1.length()-1 ; index_1 >0; --index_1)
    {
        if (num1[index_1] > '9')
        {
            num1[index_1] -= 10;
            num1[index_1 -1 ] += 1;
        }
    }
    if (num1[0] > '9')
    {
        // If msb is >9, reduce it and add 1, Max LSB that can be added is 1
        num1[0] -= 10;
        num1 = "1"+ num1; 
    }
    
    //DEBUG//
    // cout << "num1: " << num1 << endl;
    // cout << "num1_dup: " << num1_dup << endl;
    // cout << "num2: " << num2 << endl;
    return num1;
}


int main()
{
    // 1. Storing the nums in a string to accomodate the long bytes
    string num1, num2;
    
    //DEBUG//
    //cout <<"'0' = " << int ('0') << endl;
    //cout <<"'9' = " << int ('9') << endl;

    // 2. Parsing all the arguments-----------------------------------
    while (true)
    {
        // 2. Reading the arguments-----------------------------------
        // TODO: handle unexpected input types
        cin >> num1 >> num2;
        //DEBUG// cout << "Num1: " << num1 << " Num2: " << num2 << endl;

        // 2.1 Break the loop when there are no inputs
        //DEBUG// cout<< num1.length() << " | " << num2.length() << endl;
        if ( num1.empty() || num2.empty()) 
            break;

        string output = addString(num1, num2);
        cout << output <<endl;

        // Resetting the string so that the string gets flushed
        num1.clear();
        num2.clear();
    }
    // -----------------------------------------------------------
    return 0;
}
