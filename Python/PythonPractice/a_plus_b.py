__author__ = "Pranav Prakash Jha"
'''
File: aPlusb.cpp
Purpose: https://www.hackerearth.com/practice/basic-programming/complexity-analysis/time-and-space-complexity/practice-problems/algorithm/a-b-4/description/

Problem Description
Given a series of integer pairs A and B, output the sum of A and B.

Input Format
Each line contains two integers, A and B. One input file may contain several pairs  where $$0 \leq P \leq 12$$.

Output Format
Output a single integer per line - The sum of A and B.

Constraints
$$0 <= A,B <= 10^{98}$$

Test Cases:
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
1983479127394712739417293471297349712347812 12938471297349127394791273497123471297341927
9283479273497239472934729378492734 2398472938479237492374927947293749274
19191919191919191 19191919191919191919191919191919191919191919191919191919191919191919191
98919283791723912793719873971293791283791273123 1928371927391723971927391737172937
0 0
0 1
'''

while True:
    try:
        num1,num2=map(int, raw_input().split())
        sum=num1+num2
        print (sum)
    except EOFError:
        break
